// jQuery Plugin Boilerplate
// A boilerplate for jumpstarting jQuery plugins development
// version 1.1, May 14th, 2011
// by Stefan Gabos

// remember to change every instance of "pluginName" to the name of your plugin!
; (function ($, window, document, undefined) {

    // here we go!
    $.zoom360 = function (element, options) {

        // plugin's default options
        // this is private property and is  accessible only from inside the plugin
        var defaults = {
            basename: '',
            filetype: 'jpg',
            path: '',
            separator: '_',
            total: 0,
            width: 200,
            height: 400,
            title: 'Yet another 360 Zoom Spinner',
            preload: null,
            canvas: null,
            context: null,
            bgImage: null,
            bgBitmap: null,
            SCALE_Y: null,
            SCALE_X: null,
            images: {},
            page: 10,
            currImg: 1,
            currPos: 0,
            initPos: 0,
            lastPos: 0,
            clicked: false,
            first: true
        };


        // to avoid confusions, use "plugin" to reference the 
        // current instance of the object
        var plugin = this;

        // this will hold the merged default, and user-provided options
        // plugin's properties will be available through this object like:
        // plugin.settings.propertyName from inside the plugin or
        // element.data('pluginName').settings.propertyName from outside the plugin, 
        // where "element" is the element the plugin is attached to;
        plugin.settings = {}

        var $element = $(element), // reference to the jQuery version of DOM element
             element = element;    // reference to the actual DOM element

        var hammer;

        // the "constructor" method that gets called when the object is created
        plugin.init = function () {

            // the plugin's final properties are the merged default and 
            // user-provided options (if any)
            plugin.settings = $.extend({}, defaults, options);

            // code goes here
            preloadImages($element, plugin.settings);
        }

        // public methods
        // these methods can be called like:
        // plugin.methodName(arg1, arg2, ... argn) from inside the plugin or
        // element.data('pluginName').publicMethod(arg1, arg2, ... argn) from outside 
        // the plugin, where "element" is the element the plugin is attached to;
        plugin.show = function () {

        };
        plugin.hide = function () {

        };
        plugin.toggle = function () {

        };
        plugin.update = function (content) { /* !!! */ };
        plugin.destroy = function () {
            $.each(plugin.settings.images, function(i, item){
                $(item).remove();
            });

            $element.unbind();
            plugin.settings.context.clearRect(0, 0, element.width, element.height);
            plugin.settings.images = {};
            plugin.settings.preload.close();
            plugin.settings.preload = null;
        };

        // private methods
        var preloadImages = function (el, options) {
            // some logic
            options.preload = new createjs.PreloadJS();
            options.preload.setMaxConnections(10);
            options.preload.onComplete = initZoom;
            options.preload.onFileLoad = handleFileLoad;

            var manifest = [];

            for (var j = 1; j <= 20; j++) {
                var imgSrc = options.path + options.basename + options.separator + j + "." + options.filetype;
                var _id = options.basename + "_pic" + j;

                if ($.inArray(_id, options.images) == -1)
                    manifest.push({ id: _id, src: imgSrc });
            }

            for (var j = options.total; j >= (options.total - 20) ; j--) {
                var imgSrc = options.path + options.basename + options.separator + j + "." + options.filetype;
                var _id = options.basename + "_pic" + j;

                if ($.inArray(_id, options.images) == -1)
                    manifest.push({ id: _id, src: imgSrc });
            }

            options.lastLeft = 20;
            options.lastRight = (options.total - 20);

            options.preload.loadManifest(manifest);

            options.canvas = element;
            options.context = options.canvas.getContext("2d");
        };

        var handleFileLoad = function (o) {
            if (o.type == "image") {
                plugin.settings.images[o.id] = o.result;
            }
        };

        var initZoom = function () {
            if (!plugin.settings.first) { return; }
            plugin.settings.first = false;

            $element.bind("mousedown touchstart", getCurrentPos);
            $element.bind("mouseup touchend mouseout", stopMoving);
            $element.bind("mousemove touchmove", rotate);
            
            var pic = plugin.settings.basename + plugin.settings.separator + "pic1";
            plugin.settings.bgImage = plugin.settings.images[pic];

            changeImage(plugin.settings.bgImage);
        };

        var loadMoreLeft = function (last) {
            var manifest = [];

            for (var j = last; j <= (last + 10) ; j++) {
                var imgSrc = plugin.settings.path + plugin.settings.basename + plugin.settings.separator + j + "." + plugin.settings.filetype;
                var _id = plugin.settings.basename + "_pic" + j;

                if ($.inArray(_id, plugin.settings.images) == -1)
                    manifest.push({ id: _id, src: imgSrc });
            }

            plugin.settings.lastLeft += 10;
            plugin.settings.preload.close();
            plugin.settings.preload.loadManifest(manifest);
        };

        var loadMoreRight = function (last) {
            var manifest = [];

            for (var j = last; j >= (last - plugin.settings.page) ; j--) {
                var imgSrc = plugin.settings.path + plugin.settings.basename + plugin.settings.separator + j + "." + plugin.settings.filetype;
                var _id = plugin.settings.basename + "_pic" + j;

                if ($.inArray(_id, plugin.settings.images) == -1)
                    manifest.push({ id: _id, src: imgSrc });
            }

            plugin.settings.lastRight -= 10;
            plugin.settings.preload.close();
            plugin.settings.preload.loadManifest(manifest);
        };

        var getCurrentPos = function (e) {
            if (e.type == "touchstart") {
                plugin.settings.currPos = window.event.touches[0].pageX;
            } else {
                plugin.settings.currPos = e.pageX;
            }
            plugin.settings.clicked = true;
            return false;
        };

        var stopMoving = function (e) {
            plugin.settings.clicked = false;
        };

        var rotate = function (e) {
            if (plugin.settings.clicked) {
                var pageX = e.rawX;
                if (e.type == "touchmove") {
                    pageX = window.event.targetTouches[0].pageX;
                } else {
                    pageX = e.pageX;
                }
                var total = parseInt(plugin.settings.total);
                var width_step = 20;
                if (Math.abs(plugin.settings.currPos - pageX) >= width_step) {
                    if (plugin.settings.currPos - pageX >= width_step) {
                        plugin.settings.currImg++;
                        if (plugin.settings.currImg > total) {
                            plugin.settings.currImg = 1;
                        }
                    } else {
                        plugin.settings.currImg--;
                        if (plugin.settings.currImg < 1) {
                            plugin.settings.currImg = total;
                        }
                    }
                    plugin.settings.currPos = pageX;
                }

                var _id = plugin.settings.basename + "_pic" + plugin.settings.currImg;
                changeImage(plugin.settings.images[_id]);

                if ((plugin.settings.lastLeft - plugin.settings.currImg) == (plugin.settings.page / 2)) {
                    loadMoreLeft(plugin.settings.lastLeft);
                }

                if ((plugin.settings.currImg - plugin.settings.lastRight) == (plugin.settings.page / 2)) {
                    loadMoreRight(plugin.settings.lastRight);
                }
            }
        };       

        var changeImage = function (image) {
            if (image == undefined)
                return;

            var ratio = image.naturalWidth / image.naturalHeight;

            //if you want change Height:
            var targetWidth = element.height * ratio;

            //if you want change Width:
            var targetHeight = element.width * ratio;

            var centerW = (900 - targetWidth) / 2;
            var centerH = (760 - targetHeight) / 2;

            plugin.settings.context.drawImage(image, centerW, centerH, targetWidth, targetHeight);
        };       

        // fire up the plugin!
        // call the "constructor" method
        plugin.init();

    }

    // add the plugin to the jQuery.fn object
    $.fn.zoom360 = function (options) {

        // iterate through the DOM elements we are attaching the plugin to
        return this.each(function () {

            // if plugin has not already been attached to the element
            if (undefined == $(this).data('zoom360')) {

                // create a new instance of the plugin
                // pass the DOM element and the user-provided options as arguments
                var plugin = new $.zoom360(this, options);

                // in the jQuery version of the element
                // store a reference to the plugin object
                // you can later access the plugin and its methods and properties like
                // element.data('pluginName').publicMethod(arg1, arg2, ... argn) or
                // element.data('pluginName').settings.propertyName
                $(this).data('zoom360', plugin);

            }

        });

    }

})(jQuery, window, document);
