/**
* We wrap all our code in the jQuery "DOM-ready" function to make sure the script runs only
* after all the DOM elements are rendered and ready to take action
*/
var threesixty = {
    // Tells if the app is ready for user interaction
    ready: false,
    // Tells the app if the user is dragging the pointer
    dragging: false,
    // Stores the pointer starting X position for the pointer tracking
    pointerStartPosX: 0,
    // Stores the pointer ending X position for the pointer tracking
    pointerEndPosX: 0,
    // Stores the distance between the starting and ending pointer X position in each time period we are tracking the pointer
    pointerDistance: 0,

    // The starting time of the pointer tracking period
    monitorStartTime: 0,
    // The pointer tracking time duration
    monitorInt: 10,
    // A setInterval instance used to call the rendering function
    ticker: 0,
    // Sets the speed of the image sliding animation
    speedMultiplier: 10,
    // CanvasLoader instance variable
    spinner: null,

    // Stores the total amount of images we have in the sequence
    totalFrames: 180,
    // The current frame value of the image slider animation
    currentFrame: 0,
    // Stores all the loaded image objects
    frames: [],
    // The value of the end frame which the currentFrame will be tweened to during the sliding animation
    endFrame: 0,
    // We keep track of the loaded images by increasing every time a new image is added to the image slider
    loadedImages: 0,
    /* image information*/
    basename: '',
    filetype: '',
    path: '',
    separator: '',
    //MsGesture-> touch events
    myGesture: null,
    //DOM element
    element: null,
    // jquery element
    $element: null,

    /**
	* Adds a "spiral" shaped CanvasLoader instance to the #spinner div
	*/
    addSpinner: function () {
        threesixty.spinner = new CanvasLoader("spinner");
        threesixty.spinner.setShape("spiral");
        threesixty.spinner.setDiameter(90);
        threesixty.spinner.setDensity(90);
        threesixty.spinner.setRange(1);
        threesixty.spinner.setSpeed(4);
        threesixty.spinner.setColor("#333333");
        // As its hidden and not rendering by default we have to call its show() method
        threesixty.spinner.show();
        // We use the jQuery fadeIn method to slowly fade in the preloader
        $("#spinner").fadeIn("slow");
    },

    /**
	* Creates a new <li> and loads the next image in the sequence inside it.
	* With jQuery we add the "load" event handler to the image, so when it's successfully loaded, we call the "imageLoaded" function.
	*/
    loadImage: function () {
        // Creates a new <li>
        var li = document.createElement("li");
        // Generates the image file name using the incremented "loadedImages" variable
        var imageName = threesixty.path + threesixty.basename + threesixty.separator + (threesixty.loadedImages + 1) + "." + threesixty.filetype;
        //var imageName = "img/threesixty_" + (loadedImages + 1) + ".jpg";
        /*
			Creates a new <img> and sets its src attribute to point to the file name we generated.
			It also hides the image by applying the "previous-image" CSS class to it.
			The image then is added to the <li>.
		*/
        var image = $('<img>').attr('src', imageName).addClass("previous-image").appendTo(li);
        // We add the newly added image object (returned by jQuery) to the "frames" array.
        threesixty.frames.push(image);
        // We add the <li> to the <ol>
        $("#threesixty_images").append(li);
        /*
			Adds the "load" event handler to the new image.
			When the event triggers it calls the "imageLoaded" function.
		*/
        $(image).load(function () {
            threesixty.imageLoaded();
        });
    },

    /**
	* It handles the image "load" events.
	* Each time this function is called it checks if all the images have been loaded or it has to load the next one.
	* Every time a new image is succesfully loaded, we set the percentage value of the preloader to notify the user about the loading progress.
	* If all the images are loaded, it hides the preloader using the jQuery "fadeOut" method, which on complete stops the preloader rendering
	* and calls the "showThreesixty" method, that displays the image slider.
	*/
    imageLoaded: function () {
        // Increments the value of the "loadedImages" variable
        threesixty.loadedImages++;
        // Updates the preloader percentage text
        $("#spinner span").text(Math.floor(threesixty.loadedImages / threesixty.totalFrames * 100) + "%");
        // Checks if the currently loaded image is the last one in the sequence...
        if (threesixty.loadedImages == threesixty.totalFrames) {
            // ...if so, it makes the first image in the sequence to be visible by removing the "previous-image" class and applying the "current-image" on it
            threesixty.frames[0].removeClass("previous-image").addClass("current-image");
            /*
                Displays the image slider by using the jQuery "fadeOut" animation and its complete event handler.
                When the preloader is completely faded, it stops the preloader rendering and calls the "showThreesixty" function to display the images.
            */
            $("#spinner").fadeOut("slow", function () {
                threesixty.spinner.hide();
                threesixty.showThreesixty();
            });
        } else {
            // ...if not, Loads the next image in the sequence
            threesixty.loadImage();
        }
    },

    /**
    * Displays the images with the "swooshy" spinning effect.
    * As the endFrame is set to -720, the slider will take 4 complete spin before it stops.
    * At this point it also sets the application to be ready for the user interaction.
    */
    showThreesixty: function () {
        // Fades in the image slider by using the jQuery "fadeIn" method
        $("#threesixty_images").fadeIn("slow");
        // Sets the "ready" variable to true, so the app now reacts to user interaction 
        threesixty.ready = true;
        // Sets the endFrame to an initial value...
        threesixty.endFrame = -720;
        // ...so when the animation renders, it will initially take 4 complete spins.
        threesixty.refresh();
    },

    /**
    * Renders the image slider frame animations.
    */
    render: function () {
        // The rendering function only runs if the "currentFrame" value hasn't reached the "endFrame" one
        if (threesixty.currentFrame !== threesixty.endFrame) {
            /*
                Calculates the 10% of the distance between the "currentFrame" and the "endFrame".
                By adding only 10% we get a nice smooth and eased animation.
                If the distance is a positive number, we have to ceil the value, if its a negative number, we have to floor it to make sure
                that the "currentFrame" value surely reaches the "endFrame" value and the rendering doesn't end up in an infinite loop.
            */
            var frameEasing = threesixty.endFrame < threesixty.currentFrame ? Math.floor((threesixty.endFrame - threesixty.currentFrame) * 0.1) : Math.ceil((threesixty.endFrame - threesixty.currentFrame) * 0.1);
            // Sets the current image to be hidden
            threesixty.hidePreviousFrame();
            // Increments / decrements the "currentFrame" value by the 10% of the frame distance
            threesixty.currentFrame += frameEasing;
            // Sets the current image to be visible
            threesixty.showCurrentFrame();
        } else {
            // If the rendering can stop, we stop and clear the ticker
            window.clearInterval(threesixty.ticker);
            threesixty.ticker = 0;
        }
    },

    /**
    * Creates a new setInterval and stores it in the "ticker"
    * By default I set the FPS value to 60 which gives a nice and smooth rendering in newer browsers
    * and relatively fast machines, but obviously it could be too high for an older architecture.
    */
    refresh: function () {
        // If the ticker is not running already...
        if (threesixty.ticker === 0) {
            // Let's create a new one!
            threesixty.ticker = self.setInterval(threesixty.render, Math.round(1000 / 60));
        }
    },

    /**
    * Hides the previous frame
    */
    hidePreviousFrame: function () {
        /*
            Replaces the "current-image" class with the "previous-image" one on the image.
            It calls the "getNormalizedCurrentFrame" method to translate the "currentFrame" value to the "totalFrames" range (1-180 by default).
        */
        threesixty.frames[threesixty.getNormalizedCurrentFrame()].removeClass("current-image").addClass("previous-image");
    },

    /**
    * Displays the current frame
    */
    showCurrentFrame: function () {
        /*
            Replaces the "current-image" class with the "previous-image" one on the image.
            It calls the "getNormalizedCurrentFrame" method to translate the "currentFrame" value to the "totalFrames" range (1-180 by default).
        */
        var img = threesixty.frames[threesixty.getNormalizedCurrentFrame()];

        var ratio = img.naturalWidth / img.naturalHeight;

        //if you want change Height:
        var targetWidth = 900 * ratio;

        //if you want change Width:
        var targetHeight = 760 * ratio;

        var centerW = (900 - targetWidth) / 2;
        var centerH = (760 - targetHeight) / 2;

        $(img).width(centerW + 'px');
        $(img).height(centerH + 'px');

        img.removeClass("previous-image").addClass("current-image");
    },

    /**
    * Returns the "currentFrame" value translated to a value inside the range of 0 and "totalFrames"
    */
    getNormalizedCurrentFrame: function () {
        var c = -Math.ceil(threesixty.currentFrame % threesixty.totalFrames);
        if (c < 0) c += (threesixty.totalFrames - 1);
        return c;
    },

    /**
    * Returns a simple event regarding the original event is a mouse event or a touch event.
    */
    getPointerEvent: function (event) {
        return event.originalEvent.targetTouches ? event.originalEvent.targetTouches[0] : event;
    },

    init: function (basename, filetype, path, separator) {
        this.destroy();

        threesixty.basename = basename;
        threesixty.filetype = filetype;
        threesixty.path = path;
        threesixty.separator = separator;

        /*
		    We launch the application by...
		    Adding the preloader, and...
	    */
        threesixty.addSpinner();
        // loading the firt image in the sequence.
        threesixty.loadImage();

        /**
        * Adds the jQuery "mousedown" event to the image slider wrapper.
        */
        $("#threesixty").mousedown(function (event) {
            // Prevents the original event handler behaciour
            event.preventDefault();
            // Stores the pointer x position as the starting position
            threesixty.pointerStartPosX = threesixty.getPointerEvent(event).pageX;
            // Tells the pointer tracking function that the user is actually dragging the pointer and it needs to track the pointer changes
            threesixty.dragging = true;
        });

        /**
        * Adds the jQuery "mouseup" event to the document. We use the document because we want to let the user to be able to drag
        * the mouse outside the image slider as well, providing a much bigger "playground".
        */
        $(document).mouseup(function (event) {
            // Prevents the original event handler behaciour
            event.preventDefault();
            // Tells the pointer tracking function that the user finished dragging the pointer and it doesn't need to track the pointer changes anymore
            threesixty.dragging = false;
        });

        /**
        * Adds the jQuery "mousemove" event handler to the document. By using the document again we give the user a better user experience
        * by providing more playing area for the mouse interaction.
        */
        $(document).mousemove(function (event) {
            // Prevents the original event handler behaciour
            event.preventDefault();
            // Starts tracking the pointer X position changes
            threesixty.trackPointer(event);
        });

        /**
        *
        */
        $("#threesixty").live("touchstart", function (event) {
            // Prevents the original event handler behaviour
            event.preventDefault();
            // Stores the pointer x position as the starting position
            threesixty.pointerStartPosX = threesixty.getPointerEvent(event).pageX;
            // Tells the pointer tracking function that the user is actually dragging the pointer and it needs to track the pointer changes
            threesixty.dragging = true;
        });

        /**
        *
        */
        $("#threesixty").live("touchmove", function (event) {
            // Prevents the original event handler behaciour
            event.preventDefault();
            // Starts tracking the pointer X position changes
            threesixty.trackPointer(event);
        });

        /**
        *
        */
        $("#threesixty").live("touchend", function (event) {
            // Prevents the original event handler behaciour
            event.preventDefault();
            // Tells the pointer tracking function that the user finished dragging the pointer and it doesn't need to track the pointer changes anymore
            threesixty.dragging = false;
        });

        $element = $("#threesixty");
        element = document.getElementById("threesixty");
        //element.addEventListener("MSGestureStart", threesixty.pointerEventListener, false);
        //element.addEventListener("MSGestureEnd", threesixty.pointerEventListener, false);
        element.addEventListener("MSGestureChange", threesixty.pointerEventListener, false);

        threesixty.myGesture = new MSGesture();
        threesixty.myGesture.target = element;

        element.addEventListener("MSPointerDown", function (e) {
            threesixty.myGesture.addPointer(e.pointerId);
        });   
    },    

    pointerEventListener: function (e) {

        //if (evt.type == "MSGestureChange") {
            //We're going to scale the X and Y coordinates by the same amount
            //var cssScale = "scale(" + evt.scale + ")";
            //$element.css("transform", cssScale);

            var m = new MSCSSMatrix(e.target.style.transform); // Get the latest CSS transform on the element
            e.target.style.transform = m
            .translate(e.offsetX, e.offsetY) // Move the transform origin under the center of the gesture
            .rotate(e.rotation * 180 / Math.PI) // Apply Rotation
            .scale(e.scale) // Apply Scale
            .translate(e.translationX, e.translationY) // Apply Translation
            .translate(-e.offsetX, -e.offsetY); // Move the transform origin back
        //}        

        //evt.stopPropagation();
    },

    /**
    * Tracks the pointer X position changes and calculates the "endFrame" for the image slider frame animation.
    * This function only runs if the application is ready and the user really is dragging the pointer; this way we can avoid unnecessary calculations and CPU usage.
    */
    trackPointer: function (event) {
        // If the app is ready and the user is dragging the pointer...
        if (threesixty.ready && threesixty.dragging) {
            // Stores the last x position of the pointer
            threesixty.pointerEndPosX = threesixty.getPointerEvent(event).pageX;
            // Checks if there is enough time past between threesixty and the last time period of tracking
            if (threesixty.monitorStartTime < new Date().getTime() - threesixty.monitorInt) {
                // Calculates the distance between the pointer starting and ending position during the last tracking time period
                threesixty.pointerDistance = threesixty.pointerEndPosX - threesixty.pointerStartPosX;
                // Calculates the endFrame using the distance between the pointer X starting and ending positions and the "speedMultiplier" values
                threesixty.endFrame = threesixty.currentFrame + Math.ceil((threesixty.totalFrames - 1) * threesixty.speedMultiplier * (threesixty.pointerDistance / $("#threesixty").width()));
                // Updates the image slider frame animation
                threesixty.refresh();
                // restarts counting the pointer tracking period
                threesixty.monitorStartTime = new Date().getTime();
                // Stores the the pointer X position as the starting position (because we started a new tracking period)
                threesixty.pointerStartPosX = threesixty.getPointerEvent(event).pageX;
            }
        }
    },

    destroy: function () {
        for (var i = 0; i < this.frames.length; i++) {
            var img = this.frames[i];
            $(img).remove();
        }

        $("#threesixty_images").empty();
        $("#threesixty").unbind();

        this.ready = false;
        this.dragging = false;
        this.pointerStartPosX = 0;
        this.pointerEndPosX = 0;
        this.pointerDistance = 0;
        this.monitorStartTime = 0;
        this.monitorInt = 10;
        this.ticker = 0;
        this.speedMultiplier = 10;
        this.spinner = null;
        this.totalFrames = 180;
        this.currentFrame = 0;
        this.frames = [];
        this.endFrame = 0;
        this.loadedImages = 0;
        this.basename = '';
        this.filetype = '';
        this.path = '';
        this.separator = '';
    }
}
