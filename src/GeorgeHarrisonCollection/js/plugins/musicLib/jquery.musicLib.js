// jQuery Plugin Boilerplate
// A boilerplate for jumpstarting jQuery plugins development
// version 1.1, May 14th, 2011
// by Stefan Gabos

// remember to change every instance of "pluginName" to the name of your plugin!
; (function ($, window, document, undefined) {

    // here we go!
    $.musicLib = function (element, options) {

        // plugin's default options
        // this is private property and is  accessible only from inside the plugin
        var defaults = {
            title: 'Music Library',
            albums: null,
            guitars: null,
            titleTmpl: "<li><img src='{img}' style='vertical-align: top' />{songs}</li>",
            songTmpl: "<li>" +
                "<audio src='{itunes}'/>" +
                "<a href='#' class='showSongDetails songLink ' ><img src='/resources/images/assets/plus_sign.png' style='vertical-align: top' />" +
                "<span style='margin-left: 5px; vertical-align: top; white-space:nowrap;'>{name}</span></a>" +

                "<a href='#' class='playSong songLink ' style='display:none'><img src='/resources/images/assets/button_audiolist_play_black.png' style='vertical-align: top;' />" +
                "<span style='margin-left: 5px; vertical-align: top; white-space:nowrap;'>{name}</span></a>" +

                "<div class='song'>" +
                "<label class='songLabel'>album: </label><span class='songData' style='font-style: italic; white-space:nowrap;'>{album}</span><br/>" +
                "<label class='songLabel'>artist: </label><span class='songData' >{artist}</span><br/>" +
                "<label class='songLabel'>label: </label><span class='songData' >{label}</span><br/>" +
                "<label class='songLabel'>year: </label><span class='songData' >{year}</span><br/>" +
                "</div>" +
                "</li>",
            isVisible: false
        };


        // to avoid confusions, use "plugin" to reference the 
        // current instance of the object
        var plugin = this;

        // this will hold the merged default, and user-provided options
        // plugin's properties will be available through this object like:
        // plugin.settings.propertyName from inside the plugin or
        // element.data('pluginName').settings.propertyName from outside the plugin, 
        // where "element" is the element the plugin is attached to;
        plugin.settings = {}

        var $element = $(element), // reference to the jQuery version of DOM element
             element = element;    // reference to the actual DOM element

        // the "constructor" method that gets called when the object is created
        plugin.init = function () {

            // the plugin's final properties are the merged default and 
            // user-provided options (if any)
            plugin.settings = $.extend({}, defaults, options);

            // code goes here
            var div = "<div class='musicLibTop'>" +

                      "<div class='musicLibHeader'>" +
                      "<img src='/Resources/Images/Assets/Headline_wOrangeUnderline_MusicLibrary.png' /> " +
                      "<a href='#' id='faqBtn'><img src='/Resources/Images/Assets/icon_musiclibrary_questionmark.png' /></a> " +
                      "</div>" +

                      "<div class='musicLibSongs' id='songs'></div>" +

                      "<div class='musicLibFooter'>" +
                      "<ul class='musicLibMenu'>" +
                      "<li><a href='#' id=''><img src='/resources/images/assets/Volume-inactive.png' data-hover='/resources/images/assets/Volume-active.png' /></a></li>" +
                      "<li><a href='#' id=''><img src='/resources/images/assets/icon_rewind_off.png' data-hover='/resources/images/assets/icon_rewind_on.png' /></a></li>" +
                      "<li><a href='#' id=''><img src='/resources/images/assets/icon_play_off.png' data-hover='/resources/images/assets/icon_play_on.png' /></a></li>" +
                      "<li><a href='#' id=''><img src='/resources/images/assets/icon_forward_off.png' data-hover='/resources/images/assets/icon_forward_on.png' /></a></li>" +
                      "<li><a href='#' id=''><img src='/resources/images/assets/icon_musiclibrary_on.png' /></a></li>" +
                      "</ul>" +
                      "</div>" +
                      "<div id='faqContainer'>" +
                      "<div id='faqInner'>" +
                      "<div class='closeHeader'>" +
                      "<a href='#' id='faqCloseDlg' style='padding: 5px 5px 5px 5px'><img src='/resources/images/assets/icon_close_off.png' data-hover='/resources/images/assets/icon_close_on.png' /></a>" +
                      "</div>" +
                      "<div>" +
                      "<img src='/resources/images/assets/icon_musiclibrary_question_bubble.png' />" +
                      "</div>" +
                      "</div>" +
                      "</div>";

            $element.append(div);

            preloadSongs($element, plugin.settings);

            $(".showSongDetails").bind('click', showSongDetails);
            $(".playSong").bind('click', playSong);
            $("#faqBtn").bind('click', showFaq);

            $("#faqCloseDlg").bind('click', closeFaq);

            $('img[data-hover]').hover(function () {
                $(this)
                    .attr('tmp', $(this).attr('src'))
                    .attr('src', $(this).attr('data-hover'))
                    .attr('data-hover', $(this).attr('tmp'))
                    .removeAttr('tmp');
            }).each(function () {
                $('<img />').attr('src', $(this).attr('data-hover'));
            });
        }

        // public methods
        // these methods can be called like:
        // plugin.methodName(arg1, arg2, ... argn) from inside the plugin or
        // element.data('pluginName').publicMethod(arg1, arg2, ... argn) from outside 
        // the plugin, where "element" is the element the plugin is attached to;
        plugin.show = function () {
            $("#musicLib").animate({ width: 'toggle' }, 350);
            plugin.settings.isVisible = true;
        };
        plugin.hide = function () {
            $("#musicLib").animate({ width: 'toggle' }, 350);
            plugin.settings.isVisible = false;
        };
        plugin.toggle = function () {
            $("#musicLib").animate({ width: 'toggle' }, 350);
        };
        plugin.update = function (content) { /* !!! */ };
        plugin.destroy = function () { };

        // private methods
        var preloadSongs = function preloadSongs(el, options) {
            if (options.albums != null) {
                var ul = "<ul>";

                for (var i = 0; i < options.albums.length; i++) {
                    var album = options.albums[i];
                    if (album.songs != null) {

                        var songs = "<ul>";

                        for (var j = 0; j < album.songs.length; j++) {
                            var song = album.songs[j];
                            var li = options.songTmpl.replace(/{name}/g, song.name)
                                .replace("{album}", song.album)
                                .replace("{artist}", song.artist)
                                .replace("{label}", song.label)
                                .replace("{year}", song.year)
                                .replace("{itunes}", song.itunes)

                            songs += li;
                        }

                        songs += "</ul>";

                        var album = options.titleTmpl.replace("{img}", album.albumTitle);
                        ul += album.replace("{songs}", songs);
                    }
                }

                ul += "</ul>";
                $("#songs").append(ul);
            }
        };

        var songInDisplay = null;
        var songHeader = null;

        var showSongDetails = function () {
            if (songInDisplay != null) {
                songInDisplay.slideToggle();
                songHeader.nextAll('.playSong:first').toggle();
                songHeader.slideToggle();
            }

            songHeader = $(this);
            songHeader.toggle();
            songHeader.nextAll('.playSong:first').slideToggle();

            songInDisplay = songHeader.nextAll('.song:first');
            songInDisplay.slideToggle();

            return false;
        };

        var playSong = function () {
            return false;
        };

        var showFaq = function (event) {
            event.preventDefault();
            var container = $("#faqContainer");
            if (!container.is(":visible")) {
                $('body').css('opacity', '0.5').css('background-color', 'black');
                container.show();
            }
        };

        var closeFaq = function () {
            var container = $("#faqContainer");
            if (container.is(":visible")) {
                $('body').css('opacity', '1.0').css('background-color', 'white');
                container.hide();
            }
        };

        // fire up the plugin!
        // call the "constructor" method
        plugin.init();

    }

    // add the plugin to the jQuery.fn object
    $.fn.musicLib = function (options) {

        // iterate through the DOM elements we are attaching the plugin to
        return this.each(function () {

            // if plugin has not already been attached to the element
            if (undefined == $(this).data('musicLib')) {

                // create a new instance of the plugin
                // pass the DOM element and the user-provided options as arguments
                var plugin = new $.musicLib(this, options);

                // in the jQuery version of the element
                // store a reference to the plugin object
                // you can later access the plugin and its methods and properties like
                // element.data('pluginName').publicMethod(arg1, arg2, ... argn) or
                // element.data('pluginName').settings.propertyName
                $(this).data('musicLib', plugin);

            }

        });

    }

})(jQuery, window, document);
