﻿(function () {
    "use strict";

    var guitars = new WinJS.Binding.List();
    var catalogue = [];
    var videos = new WinJS.Binding.List();
    var photos = new WinJS.Binding.List();
    var songs = [];

    var guitarImages = [];

    WinJS.xhr({ url: "/js/data/guitars.json" }).then(
        function (response) {

            var data = JSON.parse(response.responseText);
            for (var i = 0; i < data.length; i++) {
                var guitar = data[i];

                var prefix = guitar.prefix;

                if (guitar.prefix === 'fenderbass6') {
                    prefix = "fenderbass";
                }

                guitar.imageDefault = "/Resources/Images/Guitars/" + prefix + "/" + guitar.prefix + "_list.png";
                guitar.imageAboutOff = "/Resources/Images/Guitars/" + prefix + "/" + guitar.prefix + "_white_year.png";
                guitar.imageUnderline = "/Resources/Images/Guitars/" + prefix + "/" + guitar.prefix + "_underline.png";
                guitar.imageOrange = "/Resources/Images/Guitars/" + prefix + "/" + guitar.prefix + "_orange.png";
                guitar.imageOn = "/Resources/Images/Guitars/" + prefix + "/" + guitar.prefix + "_on.png";

                guitar.detailsPage = "/pages/guitarDetails/guitarDetails.html?prefix=" + guitar.prefix;
                guitars.push(guitar);

                for (var j = 0; j < guitar.photos.length; j++) {
                    var photo = guitar.photos[j];

                    photo.large = "/resources/images/guitars/" + prefix + "/photos/" + photo.large;
                    photo.thumbnail = "/resources/images/guitars/" + prefix + "/photos/" + photo.thumbnail;

                    photos.push(photo);
                }

                if (guitar.songs != null && guitar.songs != undefined) {
                    var album = { albumTitle: guitar.imageOn, songs: guitar.songs };
                    Data.catalogue.push(album);
                }
            }
        },
        function (error) { console.log(error); },
        function (progress) { }
    );

    WinJS.xhr({ url: "/js/data/gh_catalogue.json" }).then(
        function (response) {

            var data = JSON.parse(response.responseText);
            for (var i = 0; i < data.length; i++) {
                var library = data[i];

                if (library.songs != null) {
                    var title = library.libraryTitle.replace(".png", "_on.png");
                    var sectionTitle = "/Resources/Images/Assets/titles/" + title;
                    var album = { albumTitle: sectionTitle, songs: library.songs };

                    Data.catalogue.push(album);
                }
            }
        },
        function (error) { console.log(error); },
        function (progress) { }
    );

    WinJS.xhr({ url: "/js/data/video_list.json" }).then(
        function (response) {

            var data = JSON.parse(response.responseText);
            for (var i = 0; i < data.length; i++) {
                var video = data[i];
                videos.push(video);
            }
        },
        function (error) { console.log(error); },
        function (progress) { }
    );

    WinJS.Namespace.define("Data", {
        items: guitars,
        catalogue: catalogue,
        videos: videos,
        photos: photos,
        songs: songs
    });

    //// Get a reference for an item, using the group key and item title as a
    //// unique reference to the item that can be easily serialized.
    //function getItemReference(item) {
    //    return [item.group.key, item.title];
    //}

    //// This function returns a WinJS.Binding.List containing only the items
    //// that belong to the provided group.
    //function getItemsFromGroup(group) {
    //    return list.createFiltered(function (item) { return item.group.key === group.key; });
    //}

    //// Get the unique group corresponding to the provided group key.
    //function resolveGroupReference(key) {
    //    for (var i = 0; i < groupedItems.groups.length; i++) {
    //        if (groupedItems.groups.getAt(i).key === key) {
    //            return groupedItems.groups.getAt(i);
    //        }
    //    }
    //}

    //// Get a unique item from the provided string array, which should contain a
    //// group key and an item title.
    //function resolveItemReference(reference) {
    //    for (var i = 0; i < groupedItems.length; i++) {
    //        var item = groupedItems.getAt(i);
    //        if (item.group.key === reference[0] && item.title === reference[1]) {
    //            return item;
    //        }
    //    }
    //}


})();
