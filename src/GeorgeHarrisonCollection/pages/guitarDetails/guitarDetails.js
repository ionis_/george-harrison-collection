﻿// For an introduction to the Page Control template, see the following documentation:
// http://go.microsoft.com/fwlink/?LinkId=232511

/// <reference path="/js/plugins/lib/jquery-1.8.2-win8-1.0.js" />
/// <reference path="/js/plugins/lib/jquery-ui-1.10.0.custom.min.js" />
/// <reference path="/js/plugins/zoom360/jquery.zoom360.js" />
/// <reference path="/js/plugins/musicLib/jquery.musicLib.js" />

(function () {
    "use strict";

    var lastAudioInvoked = 0, lastAudioItem;
    var guitar, poisOpen;

    var menuItems = [
        //{ title: "Playlist", image_off: "/Resources/Images/Assets/button_text_playlist_off.png", image_on: "/Resources/Images/Assets/button_text_playlist_on.png" },
        { title: "Overview", image_off: "/Resources/Images/Assets/button_text_overview_off.png", image_on: "/Resources/Images/Assets/button_text_overview_on.png" },
        { title: "Origin", image_off: "/Resources/Images/Assets/button_text_origin_off.png", image_on: "/Resources/Images/Assets/button_text_origin_on.png" },
        { title: "Facts", image_off: "/Resources/Images/Assets/button_text_factsandperspectives_off.png", image_on: "/Resources/Images/Assets/button_text_factsandperspectives_on.png" },
        { title: "Performances", image_off: "/Resources/Images/Assets/button_text_performances_off.png", image_on: "/Resources/Images/Assets/button_text_performances_on.png" },
        { title: "About", image_off: "", image_on: "" },
        { title: "Images", image_off: "/Resources/Images/Assets/button_text_images_off.png", image_on: "/Resources/Images/Assets/button_text_images_on.png" }
    ];

    var menu = new WinJS.Binding.List(menuItems);
    WinJS.Namespace.define("Menu", { item: menu });

    WinJS.UI.Pages.define("/pages/guitarDetails/guitarDetails.html", {
        // This function is called whenever a user navigates to this page. It
        // populates the page elements with the app's data.
        ready: function (element, options) {
            // TODO: Initialize the page here.            

            $("#home").click(function () {
                WinJS.Navigation.navigate("/pages/guitarCollection/guitarCollection.html");
            });

            Data.items.forEach(function (item) {
                if (item.prefix == options.prefix) {
                    guitar = item;

                    if (guitar.prefix == "uke") {
                        var menuItems = [
                            { title: "Overview", image_off: "/Resources/Images/Assets/button_text_overview_off.png", image_on: "/Resources/Images/Assets/button_text_overview_on.png" },
                            { title: "About", image_off: "/Resources/Images/Guitars/uke/uke_about_off.png", image_on: "/Resources/Images/Guitars/uke/uke_about_on.png" },
                        ];
                        var menu = new WinJS.Binding.List(menuItems);
                        Menu.item = menu;

                        var list = document.getElementById("gdMenuListView").winControl;
                        list.itemDataSource = menu.dataSource
                        list.forceLayout();
                    }
                }
            });

            if (guitar != null) {
                initView();

                $("#threesixty").zoom360({
                    'basename': guitar.prefix,
                    'filetype': 'jpg',
                    'path': '/Resources/Images/Guitars/' + Guitar.item.prefix + '/360/',
                    'separator': '_',
                    'total': 180,
                });
            }
            
            poisOpen = false;
            
            WinJS.Binding.processAll();
            WinJS.UI.processAll();            
        },

        unload: function () {
            // TODO: Respond to navigations away from this page.            

            $("#threesixty").data('zoom360').destroy();
            $("#threesixty").zoom360 = null;

            lastAudioInvoked = 0;
        },

        updateLayout: function (element, viewState, lastViewState) {
            /// <param name="element" domElement="true" />

            // TODO: Respond to changes in viewState.
            //updateDisplayOrientation();
        }
    });

    function linkClickEventHandler(eventInfo) {
        eventInfo.preventDefault();
        //    var link = eventInfo.currentTarget;
        //    WinJS.Navigation.navigate(link.href);
    }

    function initView() {
        for (var i = 0; i < guitar.mediaList.length; i++) {
            guitar.mediaList[i].name = "/resources/audio/" + guitar.prefix + "/" + guitar.mediaList[i].name;
        }

        WinJS.Namespace.define("Guitar", { item: guitar });

        for (var i = 0; i < menuItems.length; i++) {
            if (menuItems[i].title === "About") {
                menuItems[i].image_off = "/Resources/Images/Guitars/" + guitar.prefix + "/" + guitar.prefix + "_about_off.png";
                menuItems[i].image_on = "/Resources/Images/Guitars/" + guitar.prefix + "/" + guitar.prefix + "_about_on.png";
            }
        }        

        Menu.item = new WinJS.Binding.List(menuItems);
        updateLists();

        setLinkEvents();

        //$("#imageDetailedView").attr('src', guitar.imageDefault);
        $(".fancybox").fancybox({ 'showNavArrows': true, 'mouseWheel': true, 'cyclic': true });
    }

    function imageHover() {
        
    }

    function setLinkEvents() {        

        $("#backToMenu").click(function (event) {
            event.preventDefault();
            defaultState();
        });

        var contentDiv = document.getElementById("sectionContent");
        contentDiv.addEventListener("mousewheel", function (eventObject) {
            var delta = -Math.round(eventObject.wheelDelta);
            contentDiv.scrollLeft = contentDiv.scrollLeft + delta;
        });

        setTopNav();

        $("#detailedView").click(detailedView);

        //hover in, hover out
        $('img[data-hover]').hover(
            function () {
                $(this)
                    .attr('tmp', $(this).attr('src'))
                    .attr('src', $(this).attr('data-hover'))
                    .attr('data-hover', $(this).attr('tmp'))
                    .removeAttr('tmp');
            }
        );
    }

    function setTopNav() {
        var prev = getPrevItem();
        var next = getNextItem();

        $("#previousGuitar").click(function (event) {
            event.preventDefault();
            WinJS.Navigation.navigate("/pages/guitarDetails/guitarDetails.html", { prefix: prev });
        });

        $("#nextGuitar").click(function (event) {
            event.preventDefault();
            WinJS.Navigation.navigate("/pages/guitarDetails/guitarDetails.html", { prefix: next });
        });
    }

    function getNextItem() {
        var next = '';

        Data.items.forEach(function (item, i) {
            if (item.prefix == guitar.prefix) {
                if (i == Data.items.length - 1) {
                    next = Data.items.getAt(0).prefix;
                }
                else {
                    next = Data.items.getAt(i + 1).prefix;
                }
            }
        });

        return next;
    }

    function getPrevItem() {
        var prev = '';

        Data.items.forEach(function (item, i) {
            if (item.prefix == guitar.prefix) {
                if (i == 0) {
                    prev = Data.items.getAt(Data.items.length - 1).prefix;
                }
                else {
                    prev = Data.items.getAt(i - 1).prefix;
                }
            }
        });

        return prev;
    }

    function updateLists() {
        var list = document.getElementById("gdMenuListView");
        list.addEventListener("iteminvoked", itemInvoked);

        var media = new WinJS.Binding.List(guitar.mediaList);
        var mediaList = document.getElementById("gdAudioListView").winControl;
        mediaList.addEventListener("iteminvoked", audioinvoked);
        mediaList.itemDataSource = media.dataSource;
        mediaList.forceLayout();

        var songs = new WinJS.Binding.List(guitar.songs);
        var songsList = document.getElementById("songListView").winControl;
        songsList.itemDataSource = songs.dataSource;
        songsList.forceLayout();
    }

    function audioinvoked(args) {

        if (lastAudioInvoked != args.detail.itemIndex) {
            $.each($('audio'), function () {
                this.pause();
            });
        }

        $(".audioItem").each(function () {
            var _img = $(this).find('img');
            _img.attr('src', "/resources/images/assets/button_audiolist_play_black.png");

            var bar = $(this).find('.progressBar');
            bar.hide();
        });

        var item = $(args.target);
        var audio = item.find('audio')[0];
        var link = item.find('img');

        if (audio.paused) {
            link.attr('src', "/resources/images/assets/button_orange_pause.png");
            audio.play();
        }
        else {
            link.attr('src', "/resources/images/assets/button_audiolist_play_black.png");
            audio.pause();
        }

        $(audio).unbind('pause ended timeupdate');

        $(audio).bind('pause ended', function () {
            var imgLink = $(this).find('img');
            imgLink.attr('src', "/resources/images/assets/button_audiolist_play_black.png");
        });

        var bar = item.find(".progressBar");
        bar.show();

        $(audio).bind('timeupdate', function () {
            var progress = item.find(".progress")[0];
            var value = 0;
            if (audio.currentTime > 0) {
                value = Math.floor((100 / audio.duration) * audio.currentTime);
            }
            progress.style.width = value + "%";
        });
    }

    function itemInvoked(args) {
        var name = Menu.item.getAt(args.detail.itemIndex).title;

        //$("#sectionContainer").slideToggle("slow");
        //$("#sectionContainer").animate({ width: 'toggle' }, 350);
        $("#sectionContainer").show("slide", { direction: "right" }, 1000);

        $("#gdMenu").hide();
        $("#songListView").hide();
        $("#poiContainer").hide();
        $("#defaultImageContainer").show();        

        switch (name) {
            case "Overview":
                $("#sectionContent").html(Guitar.item.overview);
                $("#sectionName").attr("src", "/resources/images/assets/button_text_" + name + "_on.png");
                break;
                //case "Playlist":
                //    $("#sectionName").attr("src", "/resources/images/assets/button_text_" + name + "_on.png");
                //    //$("#sectionContent").html(function () { return Guitar.item.playlistIntro; });
                //    $("#songListView").show();
                //    break;
            case "Origin":
                $("#sectionName").attr("src", "/resources/images/assets/button_text_" + name + "_on.png");
                $("#sectionContent").html(function () { return toStaticHTML(Guitar.item.origin); });
                break;
            case "Facts":
                $("#sectionName").attr("src", "/resources/images/assets/button_text_factsandperspectives_on.png");
                $("#sectionContent").html(function () { return toStaticHTML(Guitar.item.facts); });
                break;
            case "Performances":
                $("#sectionName").attr("src", "/resources/images/assets/button_text_" + name + "_on.png");
                $("#sectionContent").html(function () { return toStaticHTML(Guitar.item.performances); });
                break;
            case "About":
                $("#sectionName").attr("src", "/Resources/Images/Guitars/" + Guitar.item.prefix + "/" + Guitar.item.prefix + "_about_on.png");
                $("#sectionContent").html(function () { return toStaticHTML(Guitar.item.about); });
                break;
            case "Images":
                $("#sectionName").attr("src", "/resources/images/assets/button_text_" + name + "_on.png");
                $("#sectionContent").html(Guitar.item.imageText);
                $("#rightColContainer").hide();
                var photos = new WinJS.Binding.List(guitar.photos);
                var photosList = document.getElementById("photosListView").winControl;
                photosList.itemDataSource = photos.dataSource;
                photosList.forceLayout();
                $("#photosContainer").show();
                break;
            default:
                defaultState();
                break;
        }
    }

    function defaultState() {
        $("#songListView").hide();
        $("#sectionContainer").hide("slide", { direction: "right" }, 1000);
        $("#sectionContent").html("");
        $("#menuListView").show();
        $("#photosContainer").hide();
        $("#rightColContainer").show();
        $("#gdMenu").slideToggle("slow");
    }

    function updateDisplayOrientation() {

        switch (Windows.Graphics.Display.DisplayProperties.currentOrientation) {

            case Windows.Graphics.Display.DisplayOrientations.landscape:
                setLandscapeView();
                break;

            case Windows.Graphics.Display.DisplayOrientations.portrait:
                setPortraitView();
                break;

            case Windows.Graphics.Display.DisplayOrientations.landscapeFlipped:
                setLandscapeView();
                break;

            case Windows.Graphics.Display.DisplayOrientations.portraitFlipped:
                setPortraitView();
                break;

            default:
                setLandscapeView();
                break;
        }
    }

    function setLandscapeView() {
        initView();
        imageHover();
    }

    function setPortraitView() {
        $("#threesixty").zoom360({
            'basename': guitar.prefix,
            'filetype': 'jpg',
            'path': '/Resources/Images/Guitars/' + Guitar.item.prefix + '/360/',
            'separator': '_',
            'total': 180,
        });
    }

    function detailedView() {
        $("#dvStackImg").toggle();
        $("#dvThreeImg").toggle();

        if (!poisOpen) {
            poisOpen = true;

            $("#defaultImageContainer").hide();
            $("#poiContainer").show();
            $("#poiImage").attr('src', guitar.imageDefault);

            var tmpl = "<li><img src='{imgSrc}' data-hover='{dataHoverSrc}'><span class='x'>{x}</span><span class='y'>{y}</span></li>";
            var tmpl1 = "<li><h3>{title}</h3>{text}</li>";

            guitar.poi.forEach(function (poi, i) {
                var points = poi.coord.split(",")

                var li = tmpl.replace('{imgSrc}', '/resources/images/assets/pin' + (i + 1) + '.png')
                             .replace('{dataHoverSrc}', '/resources/images/assets/pin' + (i + 1) + '_selected.png')
                             .replace('{x}', (points[0] - 265))
                             .replace('{y}', (points[1] - 15));

                $(li).appendTo('#poi_points');

                li = tmpl1.replace('{title}', poi.title).replace('{text}', poi.text);
                $(li).appendTo('#poi_contents');
            });

            $("#pointsofinterest").pointsOfInterest();
        }
        else {
            $("#defaultImageContainer").show();
            $("#poiContainer").hide();
            poisOpen = false;
        }

        imageHover();
    };

})();
