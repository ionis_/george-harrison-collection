﻿// For an introduction to the Page Control template, see the following documentation:
// http://go.microsoft.com/fwlink/?LinkId=232511
(function () {
    "use strict";

    WinJS.UI.Pages.define("/pages/intro/intro.html", {
        // This function is called whenever a user navigates to this page. It
        // populates the page elements with the app's data.
        ready: function (element, options) {

            WinJS.Utilities.query("button").listen("click", linkClickEventHandler, false);
            WinJS.Utilities.query("video").listen("click", linkClickEventHandler, false);

            // Add the event
            document.querySelector('video').addEventListener('canplay', playVideo);

            // TODO: Initialize the page here.
            $("#skipBtn, #introMov").click(function () {

                $("#introMov").get(0).pause(); // pause before the fade happens

                // Remove it
                document.querySelector('video').removeEventListener('canplay', playVideo);

                WinJS.Navigation.navigate("/pages/home/home.html");
            });

            $("video").bind("ended", function () {
                WinJS.Navigation.navigate("/pages/home/home.html");
            });
        },

        unload: function () {
            // TODO: Respond to navigations away from this page.
        },

        updateLayout: function (element, viewState, lastViewState) {
            /// <param name="element" domElement="true" />

            // TODO: Respond to changes in viewState.
        }
    });

    function linkClickEventHandler(eventInfo) {
        eventInfo.preventDefault();
        var link = eventInfo.target;
        WinJS.Navigation.navigate(link.href);
    }

    function playVideo() {
        this.play();
    }
})();
