﻿// For an introduction to the Page Control template, see the following documentation:
// http://go.microsoft.com/fwlink/?LinkId=232511
(function () {
    "use strict";

    var $lastItemInvoked;

    WinJS.UI.Pages.define("/pages/videoVault/videoVault.html", {
        // This function is called whenever a user navigates to this page. It
        // populates the page elements with the app's data.
        ready: function (element, options) {
            $("a").click(function () {
                return false;
            });

            // TODO: Initialize the page here.
            var list = document.getElementById("videoListView").winControl;
            list.addEventListener("iteminvoked", this._itemInvoked);

            // Add the event
            document.querySelector('video').addEventListener('canplay', playVideo);

            $('img[data-hover]').hover(function () {
                $(this)
                    .attr('tmp', $(this).attr('src'))
                    .attr('src', $(this).attr('data-hover'))
                    .attr('data-hover', $(this).attr('tmp'))
                    .removeAttr('tmp');
            }).each(function () {
                $('<img />').attr('src', $(this).attr('data-hover'));
            });

            $("#home").click(function () { WinJS.Navigation.navigate("/pages/home/home.html"); });

            $("#playlist").click(function () {

                var container = $("#musicList");
                // Toggle the slide based on its current
                // visibility.
                if (container.is(":visible")) {
                    // Hide - slide up.
                    container.slideUp(1000);
                } else {
                    // Show - slide down.
                    container.slideDown(1000);
                }
            });

            WinJS.UI.processAll();
        },

        unload: function () {
            // TODO: Respond to navigations away from this page.
        },

        updateLayout: function (element, viewState, lastViewState) {
            // <param name="element" domElement="true" />
            // TODO: Respond to changes in viewState.
        },

        _itemInvoked: function (args) {
            if ($lastItemInvoked != undefined) { 
                //$lastItemInvoked.find(".videoThumb").css('background-color', 'white').css('color', 'black');
                $lastItemInvoked.find(".videoThumb").removeClass('active');
            }

            $lastItemInvoked = $(args.target);
            $lastItemInvoked.find(".videoThumb").addClass('active');

            args.preventDefault();
            document.getElementById('videoToPlay').pause();
            var video = Data.videos.getAt(args.detail.itemIndex);
            $("#videoToPlay").attr('src', video.url);
            $("#videoTitle").attr('src', video.title);
            $("#videoTitle").show();

            args.preventDefault();
        }
    });

    function playVideo() {
        this.play();
    }
})();
