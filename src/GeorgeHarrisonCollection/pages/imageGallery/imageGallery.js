﻿// For an introduction to the Page Control template, see the following documentation:
// http://go.microsoft.com/fwlink/?LinkId=232511
(function () {
    "use strict";

    WinJS.UI.Pages.define("/pages/imageGallery/imageGallery.html", {
        // This function is called whenever a user navigates to this page. It
        // populates the page elements with the app's data.
        ready: function (element, options) {
            // TODO: Initialize the page here.
            WinJS.Utilities.query("#tocNav a").listen("click", linkClickEventHandler, false);

            $("#home").click(function () { WinJS.Navigation.navigate("/pages/home/home.html"); });

            $('img[data-hover]').hover(function () {
                $(this)
                    .attr('tmp', $(this).attr('src'))
                    .attr('src', $(this).attr('data-hover'))
                    .attr('data-hover', $(this).attr('tmp'))
                    .removeAttr('tmp');
            }).each(function () {
                $('<img />').attr('src', $(this).attr('data-hover'));
            });

            $("#playlist").click(function () {

                var container = $("#musicList");
                // Toggle the slide based on its current
                // visibility.
                if (container.is(":visible")) {
                    // Hide - slide up.
                    container.slideUp(1000);
                } else {
                    // Show - slide down.
                    container.slideDown(1000);
                }
            });

            $(".fancybox").fancybox({ 'showNavArrows': true, 'mouseWheel': true, 'cyclic': true });

            WinJS.UI.processAll();
        },

        unload: function () {
            // TODO: Respond to navigations away from this page.
        },

        updateLayout: function (element, viewState, lastViewState) {
            /// <param name="element" domElement="true" />

            // TODO: Respond to changes in viewState.
        },

        //_itemInvoked: function (args) {
        //    //var url = Data.items.getAt(args.detail.itemIndex).url;
        //    //WinJS.Navigation.navigate("/pages/guitarDetails/guitarDetails.html", { prefix: prefix });
        //}

    });

    function linkClickEventHandler(eventInfo) {
        eventInfo.preventDefault();
    }
})();
