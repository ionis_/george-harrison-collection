﻿// For an introduction to the Page Control template, see the following documentation:
// http://go.microsoft.com/fwlink/?LinkId=232511
(function () {
    "use strict";

    WinJS.UI.Pages.define("/pages/guitarCollection/guitarCollection.html", {
        // This function is called whenever a user navigates to this page. It
        // populates the page elements with the app's data.
        ready: function (element, options) {
            $("#home").click(function () { WinJS.Navigation.navigate("/pages/home/home.html"); });

            document.getElementById("gcListView").addEventListener("iteminvoked", this._itemInvoked);

            $('img[data-hover]').hover(function () {
                $(this)
                    .attr('tmp', $(this).attr('src'))
                    .attr('src', $(this).attr('data-hover'))
                    .attr('data-hover', $(this).attr('tmp'))
                    .removeAttr('tmp');
            }).each(function () {
                $('<img />').attr('src', $(this).attr('data-hover'));
            });
        },

        unload: function () {
            // TODO: Respond to navigations away from this page.
        },

        updateLayout: function (element, viewState, lastViewState) {
            /// <param name="element" domElement="true" />

            // TODO: Respond to changes in viewState.
        },

        _itemInvoked: function (args) {
            var prefix = Data.items.getAt(args.detail.itemIndex).prefix;
            WinJS.Navigation.navigate("/pages/guitarDetails/guitarDetails.html", { prefix: prefix });
        }
    });

})();
