﻿// For an introduction to the Page Control template, see the following documentation:
// http://go.microsoft.com/fwlink/?LinkId=232511
(function () {
    "use strict";

    var dataArray = [
        { type: "item", title: "1", picture: "/Resources/Images/Assets/GH-APPTOUR-1.jpg", number: "/resources/images/assets/Number-1.png", text: "Welcome to The Guitar Collection: George Harrison. From the home screen, tap any section title to begin. Tap The Collection to see George's historic guitars. If this is your first time using the app, open the Music Library to see a list of suggested songs to listen to while you explore the guitars." },
        { type: "item", title: "2", picture: "/Resources/Images/Assets/GH-APPTOUR-2.jpg", number: "/resources/images/assets/Number-2.png", text: "The Music Library presents a selection of tracks that were recorded using the guitars in the app. Add songs listed here to your iPad and they will be automatically uploaded to the Music Library. If these tracks are not on your iPad, connect to the Internet to stream iTunes previews of the songs." },
        { type: "item", title: "3", picture: "/Resources/Images/Assets/GH-APPTOUR-3.jpg", number: "/resources/images/assets/Number-3.png", text: "Press The Collection to swipe through all of the guitars in the app. Tap on a guitar for a closer look. Here you will have the option to rotate the guitar in 360, learn more about its history and physical details, play songs recorded using the guitar, and listen to exclusive audio clips of George himself." },
        { type: "item", title: "4", picture: "/Resources/Images/Assets/GH-APPTOUR-4.jpg", number: "/resources/images/assets/Number-4.png", text: "In Landscape orientation, Use the previous and next buttons to cycle through the guitars. Tap the headlines to read about the history of the guitar, view rare images, and learn more about its most famous moments. Here you can also hear rare audio from George, and email a digital postcard of the guitar to a friend." },
        { type: "item", title: "5", picture: "/Resources/Images/Assets/GH-APPTOUR-5.jpg", number: "/resources/images/assets/Number-5.png", text: "Tap Playlist to see a list of songs that were recorded using the guitar. Tap on a song title to reveal track details such as album, artist, label and year of release. Tap the play button to begin listening to the song, or, if the song is not on your iPad, to hear a preview of the track. Tap add to purchase the song from iTunes." },
        { type: "item", title: "6", picture: "/Resources/Images/Assets/GH-APPTOUR-6.jpg", number: "/resources/images/assets/Number-6.png", text: "While viewing a guitar, hold your iPad in portrait orientation to shift to a full-screen view. Tap 360° View to rotate the guitar." },
        { type: "item", title: "7", picture: "/Resources/Images/Assets/GH-APPTOUR-7.jpg", number: "/resources/images/assets/Number-7.png", text: "Detailed View invites you to explore the physical characteristics of the guitar. Tap the numbers to learn more about the guitar's parts." }
    ];

    var dataList = new WinJS.Binding.List(dataArray);

    // Create a namespace to make the data publicly
    // accessible. 
    var publicMembers =
        {
            itemList: dataList
        };
    WinJS.Namespace.define("Tour", publicMembers);

    var playingIntro = false;
    var audio;

    WinJS.UI.Pages.define("/pages/appTour/appTour.html", {
        // This function is called whenever a user navigates to this page. It
        // populates the page elements with the app's data.
        ready: function (element, options) {
            // TODO: Initialize the page here.            

            $("#home").click(function () { WinJS.Navigation.navigate("/pages/home/home.html"); });

            //audio = document.getElementById("audioSong");
            //audio.addEventListener("timeupdate", updateProgress, false);

            $('img[data-hover]').hover(function () {
                $(this)
                    .attr('tmp', $(this).attr('src'))
                    .attr('src', $(this).attr('data-hover'))
                    .attr('data-hover', $(this).attr('tmp'))
                    .removeAttr('tmp');
            }).each(function () {
                $('<img />').attr('src', $(this).attr('data-hover'));
            });

            $("#playlist").click(function () {

                var container = $("#musicList");
                // Toggle the slide based on its current
                // visibility.
                if (container.is(":visible")) {
                    // Hide - slide up.
                    container.slideUp(1000);
                } else {
                    // Show - slide down.
                    container.slideDown(1000);
                }
            });

            $("#playIntro").click(function () {

                var audio = document.getElementById("introAudio");

                if (audio.paused) {
                    $(this).attr('src', "/resources/images/assets/button_orange_pause.png");
                    audio.play();
                }
                else {
                    $(this).attr('src', "/resources/images/assets/button_audiolist_play_black.png");
                    audio.pause();
                }
            });

            WinJS.UI.processAll();
        },

        unload: function () {
            // TODO: Respond to navigations away from this page.
        },

        updateLayout: function (element, viewState, lastViewState) {
            /// <param name="element" domElement="true" />

            // TODO: Respond to changes in viewState.
        }
    });
})();
