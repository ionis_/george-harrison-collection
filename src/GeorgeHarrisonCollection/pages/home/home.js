﻿(function () {
    "use strict";    

    WinJS.UI.Pages.define("/pages/home/home.html", {
        // This function is called whenever a user navigates to this page. It
        // populates the page elements with the app's data.
        ready: function (element, options) {
            // TODO: Initialize the page here.            

            document.getElementById('toc_audio').play();

            var tocVideo = document.getElementById('tocIntroVideo');
            tocVideo.onended = function (e) {
                /*Do things here!*/
                $("#tocIntroVideo").hide();
                $("#tocIntroVideo").show();
            }
            tocVideo.play();

            //$('body').videoBG({
            //    position: "fixed",
            //    zIndex: -1,
            //    mp4: '/Resources/GH-FastMove1-WEB.mov',
            //    poster: '/Resources/Images/Assets/toc_bg_last.png'
            //});

            $('img[data-hover]').hover(function () {
                $(this)
                    .attr('tmp', $(this).attr('src'))
                    .attr('src', $(this).attr('data-hover'))
                    .attr('data-hover', $(this).attr('tmp'))
                    .removeAttr('tmp');
            }).each(function () {
                $('<img />').attr('src', $(this).attr('data-hover'));
            });

            var overlayOpacityNormal = 0.3, overlayOpacitySpecial = 0;
            
            //$("#musicLibBtn").click(function (e) {
            //    e.preventDefault();
            //});

            $("#mastheadBtn").click(function (event) {
                event.preventDefault();
                var container = $("#mastheadContainer");
                if (!container.is(":visible")) {
                    $('body').css('opacity', '0.5').css('background-color', 'black');
                    container.show();
                }
            });

            $("#mastheadCloseDlg").click(function () {
                var container = $("#mastheadContainer");
                if (container.is(":visible")) {
                    $('body').css('opacity', '1.0').css('background-color', 'white');
                    container.hide();
                }
            });

            $("#muteHomeAudio").click(function () {
                var audio = document.getElementById('toc_audio');
                if (!audio.paused) {
                    audio.pause();

                    $("#imgMuteAudio").attr('src', '/resources/images/assets/Volume-OFF-inactive.png');
                    $("#imgMuteAudio").attr('data-hover', '/resources/images/assets/Volume-OFF-active.png');
                }
                else {
                    audio.play();

                    $("#imgMuteAudio").attr('data-hover', '/resources/images/assets/Volume-OFF-inactive.png');
                    $("#imgMuteAudio").attr('src', '/resources/images/assets/Volume-OFF-active.png');
                }
            });

            WinJS.Utilities.query("#tocNav a").listen("click", linkClickEventHandler, false);

            WinJS.UI.processAll();
        }
    });

    function linkClickEventHandler(eventInfo) {
        document.getElementById('toc_audio').pause();

        eventInfo.preventDefault();
        var link = eventInfo.currentTarget;
        if (link.id == "musicLibBtn") {
            showMusicLib();
            //return false;
        }
        else {
            WinJS.Navigation.navigate(link.href);
        }
    }

    function playVideo() {
        this.play();
    }

    var $musicLib = null;

    function showMusicLib() {
        if ($musicLib == null)
            $musicLib = $("#musicLib").musicLib({ albums: Data.catalogue });

        $musicLib.data('musicLib').toggle();
    }

})();
